module Main exposing (main)

import Browser
import Color exposing (Color)
import Date
import Dict exposing (Dict)
import Html as Html exposing (Html, div, h1, input, p, text)
import Html.Attributes exposing (checked, for, id, name, type_)
import Html.Events exposing (onClick)
import Http
import Json.Decode as JD exposing (Decoder)
import LineChart
import LineChart.Area as Area
import LineChart.Axis as Axis
import LineChart.Axis.Intersection as Intersection
import LineChart.Axis.Line as AxisLine
import LineChart.Axis.Range as Range
import LineChart.Axis.Tick as Tick
import LineChart.Axis.Ticks as Ticks
import LineChart.Axis.Title as Title
import LineChart.Colors as Colors
import LineChart.Container as Container
import LineChart.Coordinate as Coordinate
import LineChart.Dots as Dots
import LineChart.Events as Events
import LineChart.Grid as Grid
import LineChart.Interpolation as Interpolation
import LineChart.Junk as Junk
import LineChart.Legends as Legends
import LineChart.Line as Line
import Ports exposing (highchartsBar, highchartsPie)
import Svg
import Svg.Attributes
import Time exposing (Month(..))



-- MODEL


type alias Model =
    { avocados : List Avocado
    , kind : String
    , regions : Dict String Color
    , hovered : Maybe Float
    , selection : Maybe Selection
    , dragging : Bool
    , hinted : Maybe Avocado
    }


type alias Flags =
    String


type alias Avocado =
    { date : String
    , averagePrice : Float
    , totalVolume : Float
    , largeBags : Float
    , smallBags : Float
    , kind : String
    , region : String
    }


init : Flags -> ( Model, Cmd Msg )
init url =
    ( { avocados = []
      , kind = "conventional"
      , regions = initRegions
      , hovered = Nothing
      , selection = Nothing
      , dragging = True
      , hinted = Nothing
      }
    , getAvocados url
    )


type alias Selection =
    { xStart : Float
    , xEnd : Float
    }


initRegions : Dict String Color
initRegions =
    Dict.fromList
        [ ( "Atlanta", Colors.blue )
        , ( "LosAngeles", Colors.green )
        , ( "Chicago", Colors.gold )
        , ( "NewYork", Colors.black )
        ]


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- UPDATE


type Msg
    = GotAvocados (Result Http.Error (List Avocado))
    | ChangedKind String
    | ToggleRegion String Color
      -- Zoom
    | DragStart Coordinate.Point
    | DragAt Coordinate.Point
    | Drop Coordinate.Point
    | LeaveChart Coordinate.Point
    | LeaveContainer Coordinate.Point
    | SelectedHint (Maybe Avocado)
    | SelectedDate (Maybe Avocado)


getSelectionXStart : Float -> Model -> Float
getSelectionXStart hovered model =
    case model.selection of
        Just selection ->
            selection.xStart

        Nothing ->
            hovered


avocadosToHighchartsPie : String -> List Avocado -> { date : String, series : List { name : String, y : Float } }
avocadosToHighchartsPie date avocados =
    { date = date
    , series =
        avocados
            |> List.filter (\x -> x.date == date)
            |> List.filter (\x -> x.region /= "TotalUS")
            |> List.map (\x -> { name = x.region ++ " " ++ x.kind, y = x.totalVolume })
    }


avocadosToHighchartsBar : String -> List Avocado -> { date : String, series : List { name : String, data : List Float } }
avocadosToHighchartsBar date avocados =
    let
        toDataByRegion f region =
            List.filter (\x -> x.region == region && x.kind == "conventional")
                >> List.head
                >> Maybe.map f
                >> Maybe.withDefault 0

        toLargeBagData list =
            [ toDataByRegion .largeBags "Atlanta" list
            , toDataByRegion .largeBags "NewYork" list
            , toDataByRegion .largeBags "LosAngeles" list
            , toDataByRegion .largeBags "Chicago" list
            ]

        toSmallBagData list =
            [ toDataByRegion .smallBags "Atlanta" list
            , toDataByRegion .smallBags "NewYork" list
            , toDataByRegion .smallBags "LosAngeles" list
            , toDataByRegion .smallBags "Chicago" list
            ]

        avocadosFiltered =
            avocados
                |> List.filter (\x -> x.date == date)
    in
    { date = date
    , series =
        [ { name = "Large Bags"
          , data = toLargeBagData avocadosFiltered
          }
        , { name = "Small Bags"
          , data = toSmallBagData avocadosFiltered
          }
        ]
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotAvocados (Ok avocados) ->
            let
                avocadosFiltered =
                    filterByRegion avocados

                date =
                    List.head avocados
                        |> Maybe.map .date
                        |> Maybe.withDefault ""
            in
            ( { model | avocados = avocadosFiltered }
            , Cmd.batch
                [ highchartsPie (avocadosToHighchartsPie date avocadosFiltered)
                , highchartsBar (avocadosToHighchartsBar date avocadosFiltered)
                ]
            )

        GotAvocados (Err _) ->
            ( model, Cmd.none )

        ChangedKind kind ->
            ( { model | kind = kind }, Cmd.none )

        ToggleRegion name color ->
            ( { model | regions = toggleRegion name color model.regions }, Cmd.none )

        DragStart _ ->
            ( { model
                | selection = Nothing
                , dragging = True
              }
            , Cmd.none
            )

        DragAt point ->
            if model.dragging then
                let
                    start =
                        getSelectionXStart point.x model

                    newSelection =
                        Selection start point.x
                in
                ( { model
                    | selection = Just newSelection
                    , hovered = Just point.x
                  }
                , Cmd.none
                )

            else
                ( { model | hovered = Just point.x }, Cmd.none )

        Drop point ->
            if point.x == getSelectionXStart point.x model then
                ( { model
                    | selection = Nothing
                    , dragging = False
                  }
                , Cmd.none
                )

            else
                ( { model | dragging = False }, Cmd.none )

        LeaveChart _ ->
            ( { model | hovered = Nothing }, Cmd.none )

        LeaveContainer _ ->
            ( { model
                | hovered = Nothing
                , dragging = False
              }
            , Cmd.none
            )

        SelectedHint avocado ->
            ( { model | hinted = avocado }, Cmd.none )

        SelectedDate avocado ->
            ( model
            , case avocado of
                Just a ->
                    Cmd.batch
                        [ highchartsPie (avocadosToHighchartsPie a.date model.avocados)
                        , highchartsBar (avocadosToHighchartsBar a.date model.avocados)
                        ]

                Nothing ->
                    Cmd.none
            )


toggleRegion : String -> Color -> Dict String Color -> Dict String Color
toggleRegion name color regions =
    if Dict.member name regions then
        Dict.remove name regions

    else
        Dict.insert name color regions


filterByRegion : List Avocado -> List Avocado
filterByRegion avocados =
    avocados
        |> List.filter
            (\a ->
                (a.region == "Atlanta")
                    || (a.region == "NewYork")
                    || (a.region == "LosAngeles")
                    || (a.region == "Chicago")
                    || (a.region == "TotalUS")
            )


getAvocados : String -> Cmd Msg
getAvocados url =
    Http.get
        { url = url
        , expect = Http.expectJson GotAvocados avocadosDecoder
        }


avocadosDecoder : Decoder (List Avocado)
avocadosDecoder =
    JD.list avocadoDecoder


avocadoDecoder : Decoder Avocado
avocadoDecoder =
    JD.map7 Avocado
        (JD.field "Date" JD.string)
        (JD.field "AveragePrice" JD.float)
        (JD.field "Total Volume" JD.float)
        (JD.field "Large Bags" JD.float)
        (JD.field "Small Bags" JD.float)
        (JD.field "type" JD.string)
        (JD.field "region" JD.string)



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ h1 [] [ text "Avocado average price" ]
        , Html.form []
            [ viewSelectKinds model.kind
            , viewSelectRegions model.regions
            ]
        , viewCharts model
        ]


viewSelectKinds : String -> Html Msg
viewSelectKinds kind =
    let
        radio string =
            div []
                [ input
                    [ type_ "radio"
                    , name "kind"
                    , id string
                    , checked (string == kind)
                    , onClick (ChangedKind string)
                    ]
                    []
                , Html.label [ for string ] [ Html.text string ]
                ]
    in
    div []
        [ p [] [ text "Kind" ]
        , radio "conventional"
        , radio "organic"
        ]


viewSelectRegions : Dict String Color -> Html Msg
viewSelectRegions regions =
    let
        viewRegion : String -> Color -> Html Msg
        viewRegion regionName color =
            div []
                [ input
                    [ type_ "checkbox"
                    , name regionName
                    , id regionName
                    , checked (Dict.member regionName regions)
                    , onClick (ToggleRegion regionName color)
                    ]
                    []
                , Html.label [ for regionName ] [ Html.text regionName ]
                ]
    in
    div []
        [ p [] [ text "Kind" ]
        , viewRegion "Atlanta" Colors.blue
        , viewRegion "LosAngeles" Colors.green
        , viewRegion "Chicago" Colors.gold
        , viewRegion "NewYork" Colors.black
        ]



-- VIEW CHARTS


viewCharts : Model -> Html Msg
viewCharts model =
    let
        lineCharts =
            model.avocados
                |> List.filter (\x -> x.kind == model.kind)
                |> List.filter (\x -> dateToInt x.date >= 736300)
                |> List.filter (\x -> dateToInt x.date <= 736700)
                |> toLineCharts model.regions

        zoomChart =
            case model.selection of
                Just selection ->
                    LineChart.viewCustom (chartZoomConfig model selection) lineCharts

                Nothing ->
                    viewPlaceholder
    in
    div []
        [ LineChart.viewCustom (chartMainConfig model) lineCharts
        , zoomChart
        ]


viewPlaceholder : Html Msg
viewPlaceholder =
    Html.div
        [ Html.Attributes.style "display" "flex"
        , Html.Attributes.style "align-items" "center"
        , Html.Attributes.style "justify-content" "center"
        , Html.Attributes.style "justify-content" "center"
        , Html.Attributes.style "margin-left" "100px"
        , Html.Attributes.style "width" "700px"
        , Html.Attributes.style "height" "300px"
        , Html.Attributes.style "background" "#EEE"
        ]
        [ Html.div []
            [ Html.text "Please select a range on the chart!" ]
        ]


toLineCharts : Dict String Color -> List Avocado -> List (LineChart.Series Avocado)
toLineCharts regions avocados =
    let
        byRegions =
            Dict.map regionToLineChart regions
                |> Dict.values
                |> List.map (\toLineChart -> toLineChart avocados)

        totalUS =
            List.filter (\a -> a.region == "TotalUS") avocados
                |> LineChart.dash Colors.pink Dots.square "TotalUs" [ 4, 2 ]
    in
    totalUS :: byRegions


regionToLineChart : String -> Color -> (List Avocado -> LineChart.Series Avocado)
regionToLineChart name color =
    List.filter (\a -> a.region == name)
        >> LineChart.line color Dots.square name


dateToInt : String -> Float
dateToInt =
    Date.fromIsoString
        >> Result.toMaybe
        >> Maybe.map Date.toRataDie
        >> Maybe.map toFloat
        >> Maybe.withDefault 0.0


chartMainConfig : Model -> LineChart.Config Avocado Msg
chartMainConfig model =
    { y = Axis.default 400 "Price ($)" .averagePrice
    , x = xAxisConfig Range.default (ticksConfig toYearMonth)
    , container = containerConfig "main-line-chart"
    , interpolation = Interpolation.default
    , intersection = Intersection.default
    , legends = Legends.default
    , events = eventsMain
    , junk = junkConfig model.hovered model.selection
    , grid = Grid.default
    , area = Area.default
    , line = Line.default
    , dots = Dots.custom (Dots.full 0)
    }


containerConfig : String -> Container.Config msg
containerConfig id =
    Container.custom
        { attributesHtml = [ Html.Attributes.style "font-family" "monospace" ]
        , attributesSvg = []
        , size = Container.static
        , margin = Container.Margin 30 200 60 100
        , id = id
        }


eventsMain : Events.Config Avocado Msg
eventsMain =
    let
        options bool =
            { stopPropagation = True
            , preventDefault = True
            , catchOutsideChart = bool
            }
    in
    Events.custom
        [ Events.onWithOptions "mousedown" (options False) DragStart Events.getData
        , Events.onWithOptions "mousemove" (options False) DragAt Events.getData
        , Events.onWithOptions "mouseup" (options True) Drop Events.getData
        , Events.onWithOptions "mouseleave" (options False) LeaveChart Events.getData
        , Events.onWithOptions "mouseleave" (options True) LeaveContainer Events.getData
        ]


chartZoomConfig : Model -> Selection -> LineChart.Config Avocado Msg
chartZoomConfig model selection =
    { y = Axis.default 400 "Price ($)" .averagePrice
    , x = xAxisConfig (xAxisRangeConfig selection) (ticksConfig toMonthDate)
    , container = containerConfig "zoom-chart"
    , interpolation = Interpolation.default
    , intersection = Intersection.default
    , legends = Legends.none
    , events = eventsZoom
    , junk =
        Junk.hoverOne model.hinted
            [ ( "x", String.fromFloat << .averagePrice )
            , ( "y", .date )
            ]
    , grid = Grid.default
    , area = Area.default
    , line = Line.default
    , dots = Dots.hoverOne model.hinted
    }


eventsZoom : Events.Config Avocado Msg
eventsZoom =
    Events.custom
        [ Events.onMouseMove SelectedHint (Events.getWithin 30)
        , Events.on "touchstart" SelectedHint (Events.getWithin 100)
        , Events.on "touchmove" SelectedHint (Events.getWithin 100)
        , Events.onMouseLeave (SelectedHint Nothing)
        , Events.onClick SelectedDate (Events.getWithin 30)
        ]


xAxisRangeConfig : Selection -> Range.Config
xAxisRangeConfig selection =
    let
        start =
            min selection.xStart selection.xEnd

        end =
            if selection.xStart == selection.xEnd then
                selection.xStart + 1

            else
                max selection.xStart selection.xEnd
    in
    Range.window start end


junkConfig : Maybe Float -> Maybe Selection -> Junk.Config Avocado msg
junkConfig hovered selection =
    Junk.custom <|
        \system ->
            { below = below system selection
            , above = above system hovered
            , html = []
            }


below : Coordinate.System -> Maybe Selection -> List (Svg.Svg msg)
below system selection =
    case selection of
        Just { xStart, xEnd } ->
            let
                attributes =
                    [ Svg.Attributes.fill "#4646461a" ]

                ( yStart, yEnd ) =
                    ( system.y.min, system.y.max )

                viewSelection =
                    Junk.rectangle system attributes xStart xEnd yStart yEnd
            in
            [ viewSelection ]

        Nothing ->
            []


above : Coordinate.System -> Maybe Float -> List (Svg.Svg msg)
above system hovered =
    case hovered of
        Just h ->
            [ Junk.vertical system [] h
            , title system
            ]

        Nothing ->
            [ title system ]


title : Coordinate.System -> Svg.Svg msg
title system =
    Junk.labelAt system system.x.max system.y.max 20 -5 "start" Colors.black "Avocado in"


xAxisConfig : Range.Config -> Ticks.Config msg -> Axis.Config Avocado msg
xAxisConfig range ticks =
    Axis.custom
        { title = Title.default "Year"
        , variable = Just << dateToInt << .date
        , pixels = 1000
        , range = range
        , axisLine = AxisLine.full Colors.black
        , ticks = ticks
        }


ticksConfig : (Int -> String) -> Ticks.Config msg
ticksConfig toString =
    Ticks.intCustom 10 (tickConfig toString)


toMonthDate : Int -> String
toMonthDate =
    (\d ->
        monthToString (Date.month d)
            ++ "-"
            ++ String.fromInt (Date.day d)
    )
        << Date.fromRataDie


monthToString : Time.Month -> String
monthToString month =
    case month of
        Jan ->
            "Jan"

        Feb ->
            "Feb"

        Mar ->
            "Mar"

        Apr ->
            "Apr"

        May ->
            "May"

        Jun ->
            "Jun"

        Jul ->
            "Jul"

        Aug ->
            "Aug"

        Sep ->
            "Sep"

        Oct ->
            "Okt"

        Nov ->
            "Nov"

        Dec ->
            "Dec"


toYearMonth : Int -> String
toYearMonth =
    (\d ->
        String.fromInt (Date.year d)
            ++ "-"
            ++ String.fromInt (Date.monthNumber d)
    )
        << Date.fromRataDie


tickConfig : (Int -> String) -> Int -> Tick.Config msg
tickConfig toString number =
    let
        label =
            Junk.label Color.black (toString number)
    in
    Tick.custom
        { position = toFloat number
        , color = Color.grey
        , width = 1
        , length = 10
        , grid = True
        , direction = Tick.negative
        , label = Just label
        }



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none
