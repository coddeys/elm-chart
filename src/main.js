const Highcharts = require("highcharts");
const { Elm } = require("./Main.elm");
const url = process.env.URL;

const app = Elm.Main.init({
    flags: url,
    node: document.querySelector("main")
});

app.ports.highchartsPie.subscribe(function(data) {
    highchartsPie(data);
});

app.ports.highchartsBar.subscribe(function(data) {
    highchartsBar(data);
});

function highchartsPie(data) {
    Highcharts.chart("containerPie", {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: "pie"
        },
        title: {
            text: `Avocado market shares, date: ${data.date}`
        },
        tooltip: {
            pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>"
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: "pointer",
                dataLabels: {
                    enabled: true,
                    format: "<b>{point.name}</b>: {point.percentage:.1f} %"
                }
            }
        },
        series: [
            {
                name: "State",
                colorByPoint: true,
                data: data.series
            }
        ]
    });
}

function highchartsBar(data) {
    console.log(data);
    Highcharts.chart("containerBar", {
        chart: {
            type: "bar"
        },
        title: {
            text: `Total Bags, date: ${data.date}`
        },
        xAxis: {
            categories: [
                "Atlanta",
                "Chicago",
                "LosAngeles",
                "NewYork",
                "TotalUS"
            ]
        },
        yAxis: {
            min: 0,
            title: {
                text: "Total fruit consumption"
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: "normal"
            }
        },
        series: data.series
    });
}
