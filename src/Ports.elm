port module Ports exposing (highchartsBar, highchartsPie)


port highchartsPie : Pie -> Cmd msg


port highchartsBar : Bar -> Cmd msg


type alias Pie =
    { date : String
    , series :
        List
            { name : String
            , y : Float
            }
    }


type alias Bar =
    { date : String
    , series :
        List
            { name : String
            , data : List Float
            }
    }
