# Simple deploy action
.PHONY: deploy
deploy: clean
	mkdir -p dist
	cp db.json dist/db.json.tmp
	sed 's/{ "avocados": //' dist/db.json.tmp > dist/db.json
	head -n -1 dist/db.json > dist/db.json.tmp
	mv dist/db.json.tmp dist/db.json
	URL="/db.json" yarn build

.PHONY: clean
clean:
	rm -rf dist
