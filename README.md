# Elm Chart

## Demo

The demo version is deployed on Netlify

https://jovial-shaw-fbb37f.netlify.com/

## About

This is a simple example of using Elm language for rendering charts.

It uses two approaches
1. Pure Elm approach for rendering line-chart
2. Ports and JS library (highcharts.js) for rendering pie and bar charts

The benefit of the first approach that js source code is pretty minimum `69.44 KB` and the source code is staticly typed, so less bugs etc. But the main dowside that the library line-chart is limited.

The conclusion from using both approaches
* If the thing needs to be done pretty quick so it's better to use highcharts.js or other JS library and ports
* If the requirement to develop faster in a long run then better to use pure Elm approach

## Installation

```shell
yarn install
```

## Development


Run mock server and dev server at the same time

```shell
yarn start
```
Or run them separatly `yarn mock-server` and `yarn dev`

Open http://localhost:1234 in a browser

## Build

Build an application

```shell
yarn build
```

## Deploy


```shell
make deploy
```
